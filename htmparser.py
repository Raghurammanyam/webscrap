from bs4 import BeautifulSoup
import codecs,json
import re,os,sys
from pprint import pprint
from io import StringIO
import pandas as pd
from collections import OrderedDict
import io
from pathlib import Path
import html2text
from datetime import datetime
import xlsxwriter
import ray
import time

ray.init()

@ray.remote
def html_parser(filename,casetype=None,name=None):

    file_details ={}
    f=codecs.open(filename,'r','utf-8')
    soup = BeautifulSoup(f.read(),'lxml')

    with open(filename,'r') as ff: 

        h = html2text.HTML2Text()
        h.ignore_links = True
        h.ignore_images = True
        h.ignore_emphasis = True
        h.bypass_tables = False
        h.ignore_tables = True
        text=h.handle(ff.read())


    marked_text = [i.strip() for i in text.split('\n') if i.strip()]
    case_index = marked_text.index('## Case Details')
    status_ind= marked_text.index('## Case Status')
    petioner_ind = marked_text.index('## Petitioner and Advocate')
    case = marked_text[case_index+1:status_ind]
    status =marked_text[status_ind+1:petioner_ind]

    case_details ={}
    status = {y.split(':')[0]:re.sub('Date Not| Updated|\s{4,}','',y.split(':')[1]) for y in status}

    t=soup.find('span',{'class':'case_details_table'})
    
    case_type = re.sub('Case Type : ','',t.text)
  
    case_details['case_type'] = case_type
    x= soup.get_text(strip=True)

    
    filling_detail = re.search('(?<=Filing Number:)(.*)(?=Filing Date)',x)
    fill_no =''
    if filling_detail:
        fill_no=filling_detail.group()
    registration_detail = re.search('(?<=Registration Number:)(.*)(?=Registration Date)',x)
    
    reg_no = ''
    
    if registration_detail:
        reg_no = registration_detail.group()
    case_details['filing_Number'] = fill_no
    case_details['registration_Number'] = reg_no
    
    case_number = {k.split(':')[0]:re.sub('^\s','',k.split(':')[1]) for k in case if 'Case Code' in k or 'CNR Number' in k}
    
    case_details[list(case_number.keys())[0]] = list(case_number.values())[0]
    
    filling_date = re.search('(?<=Filing Date:)(.*)(?=Registration Number:)',x)
    
    if filling_date:
        case_details['filing_Date'] = filling_date.group()
    else:
        case_details['filing_Date'] =''
    registration_date = re.search('(?<=Registration Date:)(.*)(?=Case Code:|CNR Number:)',x)
    
    if registration_date:
        case_details['registration_Date'] = registration_date.group()
    else:
        case_details['registration_Date'] = ''

    case_status = {}

    s = soup.find('span',{'class':'Petitioner_Advocate_table'})

    file_details['case_details'] = case_details

    file_details['case_status']= status

    for script in s(["script","style"]):
        script.decompose()

    strips = list(s.stripped_strings)

    from itertools import islice,cycle

    file_details['Petitioner and Advocate'] = strips
    strips = [x for x in strips if re.search('^\d',x)]
    respondent = soup.find('span',{'class':'Respondent_Advocate_table'})
    for x in respondent(["script","style"]):
        respondent.decompose()
    respondents = list(respondent.stripped_strings)
    respondents = [re.sub('^\d\)\s|and another|and other|\s$','',x) for x in respondents]
 
    if casetype is not None:
        if casetype ==case_type.strip() or name in respondents:
            return filename
        else: 
            return False
    file_details['Respondent and Advocate'] = respondents


    act_table = soup.find("table",attrs={"class":"Acts_table"})

    order_table=soup.select('table.order_table')

    if len(order_table)>=1:
        order_text = re.search('(?<=Order Details)(.*)',order_table[0].get_text())
        if order_text:
            orders = {}
            order= re.split('\s{3,}',order_text.group().strip())
            iterloop = 0
            ordnum = []
            orddate = []
            orddetail=[]
            for x in range(len(order)//3):
                ordnum.append(order[0+iterloop])
                orddate.append(order[1+iterloop])
                orddetail.append(order[2+iterloop])
                iterloop +=3
            orders['order_no'] = ordnum
            orders['order_date'] = orddate
            orders['order_detail'] = orddetail

            file_details['Orders'] = orders

    transfer_table = soup.find("table",attrs={"class":"transfer_table"})
    
    def table_todict(table):
        history_table= {}
        try:
            for th, td in zip(cycle(table.select('th')), table.select('td')):  
                history_table.setdefault(th.text.strip(), []).append(td.text.strip())
            return history_table
        except:
            return False

    file_details['Acts'] = table_todict(act_table)
    file_details['Case Transfer Details Between The Courts'] = table_todict(transfer_table)
    file_details = ({key:value for key,value in file_details.items() if value!=False})
    df = pd.read_html(filename,attrs = {'class':'history_table'})
    act1 = df[0].to_dict()
    act1 = {x:[str(x)  for x in list(y.values())] for x,y in act1.items()}
    act1 ={key:['' if x=='nan' else x for x in value] for key,value in act1.items()}
    file_details['History of Case Hearing'] = act1
    empty ={}
    
    for x,y in file_details.items():
        if type(y) is dict:
            for z,v in y.items():
                if type(v) is list:
                    empty[x+'_'+z] = v
                else:
                    empty[x+'_'+z]=[v]
        else:
            empty[x] = y
    work_dir=os.getcwd() 
    name =Path(filename).stem

    file_name = os.path.join(work_dir,name+'.xlsx')
    workbook = xlsxwriter.Workbook(file_name)
    worksheet = workbook.add_worksheet()
    bold = workbook.add_format({'bold': 1})
    a=len(empty.keys())
    alpha_sheet = [chr(x) for x in range(65,91)]
    alpha_sheet_headrs = alpha_sheet+[x+y for x in  alpha_sheet for y in alpha_sheet]
    keys=list(empty.keys())

    for i,x in enumerate(alpha_sheet[:a]):
        worksheet.set_column(i, i, len(keys[i])+6
        
        )
        worksheet.write(x+str(1),keys[i],bold)
        for t,u in enumerate(empty[keys[i]],start=2):
            worksheet.write(x+str(t),u,bold)

    workbook.close()
    print(f"saving to html parse details to file {file_name}")
    return file_name


if __name__=='__main__':
    import time
    import concurrent.futures
    html_folder= sys.argv[1]
    paths=[]
    print("...................1st case........................................")
    casetype= (input("Enter a case_type: ")).strip()
    name = input("Enter name: ").strip()
    fileshead=[]
   
    fileshead = ray.get([html_parser.remote(os.path.join(html_folder,x),casetype,name) for x in (os.listdir(html_folder))])

    
    fileshead = [x for x in fileshead if x!=False]
    time.sleep(1)
    
    print(".....................Display top 5 files of searched string..............................")
    pprint(fileshead[:5])
    time.sleep(5)
    
    pprint("............................listing all files........................")
    time.sleep(1)
    import glob
    pprint(glob.glob(html_folder+'/*.html'))
    # pprint(paths)
    
    print("...............4th case.........................................")
    filename = input("Enter a file name: ")
    ray.get(html_parser.remote(filename))
    

