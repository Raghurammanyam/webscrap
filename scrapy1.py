from bs4 import BeautifulSoup
import requests,json
import pandas as pd
import re
products = []
prices = []
ratings = []
links =[]
for i in range(150):


    # html_page = requests.get('https://www.flipkart.com/search?sid=6bo%2Cb5g&otracker=CLP_Filters&p%5B%5D=facets.price_range.from%3DMin&p%5B%5D=facets.price_range.to%3D50000&page='+str(i))
    # print(html_page.text)
    html_page = requests.get('https://www.flipkart.com/search?sid=tyy%2C4io&otracker=CLP_Filters&p%5B%5D=facets.price_range.from%3D7000&p%5B%5D=facets.price_range.to%3DMax&page='+str(i))
    soup = BeautifulSoup(html_page.text,'html.parser')
    # print(soup.find_all('a',href=True),soup.title.string)
    for link in soup.findAll('a', attrs={'href': re.compile("^(?!https$).*")}):
        links.append(link.get('href'))
    for a in soup.findAll('a',href=True, attrs={'class':"_31qSD5"}):
        # print(a)
        # name=a.find('div', attrs={'class':'_3wU53n'})
        # price = a.find('div',attrs={'class':'_1vC4OE _2rQ-NK'})
        # rating = a.find('div',attrs={'class':'hGSR34'})
        name=a.find('div', attrs={'class':'_3wU53n'})
        price = a.find('div',attrs={'class':'_1vC4OE _2rQ-NK'})
        rating = a.find('div',attrs={'class':'hGSR34'})
        # print(rating)
        products.append(name.text)
        prices.append(price.text)
        if rating != None:
            ratings.append(rating.text)
        else:
            ratings.append(rating)
# print(prices)
# print(products)
# print(ratings)
links =['https://www.flipkart.com'+x for x in links if re.search('/p/itm',x)]
df = pd.DataFrame({'products':products,'price':prices,'ratings':ratings,'links':links})
df.to_csv('mobileprice.csv',index=False,encoding='utf-8')
