from bs4 import BeautifulSoup
import requests,json
import pandas as pd
import re
titles = []
prices =[]
links =[]
ratings=[]
for i in range(150):
    html_page = requests.get('https://www.newegg.com/global/in-en/p/pl?d=graphics%20card&Page='+str(i))
    soup = BeautifulSoup(html_page.text,'html.parser')
 
    # for link in soup.findAll('a', attrs={'href': re.compile("^(?!https$).*")}):
    #         links.append(link.get('href'))
    # links =[x for x in links if re.search('-Product$',x)]
    # print(links)
    wrap =soup.find('div', attrs={'class':"list-wrap"})

    for a in wrap.findAll('div',attrs = {'class':"item-container"}):
    #    print(a)
        b= a.select('a',href=True, attrs={'class':"item-brand"})
        c=b[0]
        links.append(c['href'])
        titles.append(c.img['title'])
        price = a.find('li',attrs={'class':"price-current"})
        prices.append(re.sub('\s+|\–|\(.*?\)','',price.text))
        rating = a.find('a',attrs={'class':"item-rating"})
        rate=re.findall('title\=.*?>',str(rating))
        print(rate)
        if len(rate)>0:
            rate=re.findall('\d+',rate[0])  
            ratings.append(rate[0]) 
        else:
                ratings.append('norating')                                
#    if len (price)>0:
#     tag=price[0]
#     rup = tag.text
#     print(rup)
print(len(titles),len(ratings))
df = pd.DataFrame({'products':titles,'price':prices,'ratings':ratings,'links':links})
df.to_csv('graphiccard.csv',index=False,encoding='utf-8')
   