# pip install pymupdf
import fitz
from fitz.utils import getColor 


color = getColor("white")

def mergedoc(filepath1,filepath2,firststring,secondstring):
    doc = fitz.open(filepath1) # opening  first file 
    doc1 = fitz.open(filepath2) # opening  second file
    
    for page in doc: 
        rl = page.searchFor("TUSCARORA HARDWOODS, INC", hit_max=1) # search for text
        r2 = page.searchFor("Bill To: Hunter Brothers Int'l, LLC",hit_max=10) #
    
        for r in rl:   # every rectangle containing this text                                     
            shape = page.newShape() 
            shape.drawRect(r) # draw an rectangle 
            shape.finish(width = 0.3, color = color, fill = color) # filled with white color 
            shape.insertTextbox(r, firststring,align=fitz.TEXT_ALIGN_CENTER,fontsize=r.y1-r.y0) # inserting text 
            shape.commit()
        for z in r2:
            shape = page.newShape()
            shape.drawRect(z) # draw an rectangle
            shape.finish(width = 0.3, color = color, fill = color) # filled with white color 
            shape.insertTextbox(z, "Bill To: "+str(secondstring),fontsize=z.y1-z.y0)
            shape.commit()
        
          
                            
    doc.insertPDF(doc1) # merging documents
    doc.save('result.pdf') # saving to new file

mergedoc('Python- 01.pdf','Python-02.pdf','gema soft inc','alpha')